#!/bin/bash

# We get the name of the current user
user=$1

# We get the current date and format it
currentDate=$(date +'%Y_%m_%d_%H_%M')

dirToBackup=/home/${user}/BUT/S1/R1_04/TP_06

# We store the backup file path
backupFile=/tmp/${user}_home_${currentDate}.tar.gz

function number_of_files {

	echo "Number of files to be included : $(find ${dirToBackup} -type f 2> /dev/null | wc -l)" 

}

function number_of_directories {

	echo "Number of directories to be included : $(find ${dirToBackup} -type d 2> /dev/null | wc -l)" 

}

# We call the function to count the number of files
number_of_files

# We call the function to count the number of directories
number_of_directories

# We compress the designated directory
tar -czvf $backupFile $dirToBackup

# We list the content of the compressed file
tar -ztvf ${backupFile}
