#!/bin/bash

if [ $# -eq 2 ]
then
	
	CONCAT=$1+$2

	echo $CONCAT

else

	echo "Mauvais nombre de paramètres en entrée !"

	exit 1

fi 
