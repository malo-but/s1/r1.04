#!/bin/bash

if [ $# -eq 2 ]
then

	USERNAME=$1

	PASSWD=$2

	sudo grep -q "$USERNAME" /etc/passwd

	if [ $? -eq $SUCCESS ] 
	then	
	
		echo "L'utilisateur $USERNAME existe déjà."

		exit 1

	fi  


	sudo useradd -d /home/"$USERNAME" -m -g users -s /bin/bash "$USERNAME"

	sudo passwd $USERNAME

	echo "L'utilisateur $USERNAME à été créé."

else

	echo  "Mauvais nombre d'arguments !"

	exit 1

fi