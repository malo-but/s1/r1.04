#!/bin/bash

if [ $# -eq 2 ]
then

	DIR="/home/$1"

	USER_UID="$(id -g $1)"

	if [ -e $DIR -a $2 -eq $USER_UID ]
	then

		echo "L'utilisateur $1 a pour UID : $USER_UID"

	fi

else

	echo "Mauvais nombre d'arguments !"

	exit 1

fi