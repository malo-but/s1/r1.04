#!/bin/bash

if [ $# -eq 1 ]
then

	if [ -s $1 ]
	then

		if [ -d $1 ]
		then

			echo "####### fichiers dans $1"

			for FILE in "$1"/*
			do

				if [ -f $FILE ]
				then

					echo "$FILE"

				fi
			
			done

			echo "####### répertoires dans $1"

			for DIR in "$1"/*
			do

				if [ -d $DIR ]
				then

					echo "$DIR"

				fi
			
			done

		else

			echo "Le fichier spécifié n'est pas un répertoire !"

			exit 0

		fi

	else

		echo "Le fichier spécifié n'existe pas ou est vide !"

		exit 0

	fi

else

	echo "Mauvais nombre d'arguments !"

	exit 0

fi