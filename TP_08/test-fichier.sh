#!/bin/bash

if [ $# -eq 1 ]
then

	FILE=$1

	CONTENT="vide"

	TYPE=""

	PERMS=""

	OWNER=""

	if [ -e $FILE ]
	then

		if [ -s $FILE ]
		then

			CONTENT="non vide"

		fi

		if [ -f $FILE ]
		then

			TYPE="fichier"

		else

			if [ -d $FILE ]
			then

				TYPE="répertoire"

			fi

		fi

		if [ -z $TYPE ]
		then

			TYPE="autre"

		fi

		if [ -r $FILE ]
		then

			PERMS="lecture"

		fi

		if [ -w $FILE ]
		then

			PERMS="$PERMS ecriture"

		fi

		if [ -x $FILE ]
		then

			PERMS="$PERMS execution"

		fi

		OWNER=$(stat -c “%U” $FILE)

		echo "Le fichier $FILE est un $TYPE $CONTENT accessible par $OWNER en $PERMS"

	else

		echo "Le fichier/repertoire n'existe pas !"

		exit 0

	fi

else

	echo "Mauvais nombre de paramètres !"

	exit 0

fi
