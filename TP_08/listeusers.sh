#!/bin/bash

if [ $# -eq 0 ]
then

	awk  -F':' '$3>99 {print User $1}' /etc/passwd | column -t | grep -v nobody

else

    echo "Erreur, vous ne devez pas entrer de paramètres"

fi