#!/bin/bash

echo "Entrez U pour utiliser votre répertoire personnel ou G pour utiliser le répertoire de votre groupe :"

read dir

if [ "$dir" = "U" ]; then

    cd $HOME

elif [ "$dir" = "G" ]; then

    cd /home/$(id -ng $USER)

fi
